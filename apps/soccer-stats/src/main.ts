import { APP_INITIALIZER, enableProdMode } from '@angular/core';

import { environment } from './environments/environment';
import { AppComponent } from './app/app.component';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideTranslocoRoot } from '@bsc/shared/util-translation-config';
import { provideAuthz } from '@bsc/shared/data-access-authz';
import { provideAuth } from '@bsc/shared/data-access-auth';
import {
  MaterialNotificationService,
  provideNotificationService
} from '@bsc/shared/util-async-helpers';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { reducers } from '@bsc/soccer-stats/data-access-app-state';
import { provideStore } from '@ngrx/store';
import { provideHttpClient } from '@angular/common/http';
import { bootstrapApplication } from '@angular/platform-browser';
import { BaseUrl } from '@bsc/shared/util-config-tokens';
import { AppStartupService } from './app/app-startup.service';
import { provideRouter } from '@angular/router';
import { routes } from './app/app.routes';
import { enUS } from 'date-fns/locale';
import { MAT_DATE_LOCALE } from '@angular/material/core';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    provideAnimations(),
    provideAuth(environment.authenticationConfig),
    provideAuthz(environment.authorizationConfig),
    provideHttpClient(),
    provideNotificationService(MaterialNotificationService),
    provideRouter(routes),
    provideStore(reducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictStateSerializability: true,
        strictActionImmutability: true,
        strictActionSerializability: true
      }
    }),
    provideStoreDevtools({
      maxAge: 50,
      logOnly: environment.production
    }),
    provideTranslocoRoot('./assets/i18n'),
    {
      provide: APP_INITIALIZER,
      useFactory: (appStartupService: AppStartupService) => () =>
        appStartupService.startup(),
      deps: [AppStartupService],
      multi: true
    },
    {
      provide: BaseUrl,
      useValue: environment.baseUrl
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: enUS
    }
  ]
}).catch(err => console.error(err));
