import { Routes } from '@angular/router';

import { authorizationGuard } from '@bsc/soccer-stats/data-access-guards';
import { environment } from '../environments/environment';
import { provideTranslocoChild } from '@bsc/shared/util-translation-config';
import { provideEffects } from '@ngrx/effects';
import {
  GameEffects,
  PlayerEffects
} from '@bsc/soccer-stats/data-access-app-state';

export const routes: Routes = [
  { path: '', redirectTo: 'games', pathMatch: 'full' },
  {
    path: 'login',
    loadComponent: () =>
      import('@bsc/soccer-stats/feature-login').then(m => m.LoginPageComponent),
    providers: [provideTranslocoChild('featureLogin')]
  },
  {
    path: 'games',
    loadChildren: () =>
      import('@bsc/soccer-stats/feature-game-stats').then(m => m.routes),
    canActivate: environment.e2e ? [] : [authorizationGuard],
    providers: [
      provideEffects(GameEffects, PlayerEffects),
      provideTranslocoChild('featureGameStats')
    ]
  },
  {
    path: 'players',
    loadChildren: () =>
      import('@bsc/soccer-stats/feature-player-stats').then(m => m.routes),
    canActivate: environment.e2e ? [] : [authorizationGuard],
    providers: [
      provideEffects(GameEffects, PlayerEffects),
      provideTranslocoChild('featurePlayerStats')
    ]
  }
];
