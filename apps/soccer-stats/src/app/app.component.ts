import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';

import {
  AppState,
  getCurrentUser
} from '@bsc/soccer-stats/data-access-app-state';
import { LogInOutService } from '@bsc/soccer-stats/data-access-log-in-out';
import { RouterLink, RouterOutlet } from '@angular/router';
import { AsyncPipe } from '@angular/common';
import { TranslocoDirective } from '@ngneat/transloco';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'bsc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    RouterLink,
    RouterOutlet,
    AsyncPipe,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule
  ]
})
export class AppComponent {
  currentUser = this.store.pipe(select(getCurrentUser));

  constructor(
    private store: Store<AppState>,
    private logInOut: LogInOutService
  ) {}

  logout() {
    this.logInOut.logout();
  }
}
