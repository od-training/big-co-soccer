import { isDevMode, makeEnvironmentProviders } from '@angular/core';
import { provideTransloco, TRANSLOCO_SCOPE } from '@ngneat/transloco';
import { provideTranslocoPersistLang } from '@ngneat/transloco-persist-lang';

import { TranslationPath } from './tokens';
import { HttpLoader } from './translation-loader.service';
import { LangDefinition } from '@ngneat/transloco/lib/types';

export function provideTranslocoRoot(
  translationPath: string,
  availableLangs: LangDefinition[] = [{ id: 'en', label: 'English' }]
) {
  return makeEnvironmentProviders([
    // saves in the selected language in the given storage
    provideTranslocoPersistLang({
      storage: {
        useValue: localStorage
      }
    }),

    provideTransloco({
      config: {
        availableLangs,
        defaultLang: 'en',
        prodMode: !isDevMode()
      },
      loader: HttpLoader
    }),
    {
      provide: TranslationPath,
      useValue: translationPath
    }
  ]);
}

export function provideTranslocoChild(scopeName: string) {
  return makeEnvironmentProviders([
    {
      provide: TRANSLOCO_SCOPE,
      useValue: scopeName
    }
  ]);
}
