import { merge, Observable, of, timer } from 'rxjs';
import { catchError, filter, map, switchMap, take, tap } from 'rxjs/operators';

export enum LoadResultStatus {
  IN_PROGRESS = 'In Progress',
  RETRYING = 'Retrying',
  WAITING = 'Waiting to Retry',
  SUCCESS = 'Success',
  ERROR = 'Error'
}

export interface InProgress {
  status: LoadResultStatus.IN_PROGRESS;
}

export interface Retrying {
  status: LoadResultStatus.RETRYING;
}

export interface Waiting {
  status: LoadResultStatus.WAITING;
}

export interface RequestError {
  status: LoadResultStatus.ERROR;
  error: Error;
  willRetry: boolean;
}

export interface Success<T> {
  status: LoadResultStatus.SUCCESS;
  results: T;
}

export type RequestUpdate<T> =
  | Success<T>
  | RequestError
  | Waiting
  | Retrying
  | InProgress;

export function isSuccess<T>(ru: RequestUpdate<T>): ru is Success<T> {
  return ru.status === LoadResultStatus.SUCCESS;
}

interface LoadWithRetryOptions {
  // To retry once after failure, use attempts=2
  attempts: number;
  retryDelayMs: number;
  retryBackoffCoefficient: number;
  retryMaxDelayMs: number;
}

const DEFAULT_OPTIONS: LoadWithRetryOptions = {
  attempts: 1,
  retryDelayMs: 2000,
  retryBackoffCoefficient: 1.5,
  retryMaxDelayMs: 30000
};

const initialStatus: RequestUpdate<unknown> = {
  status: LoadResultStatus.IN_PROGRESS
};

const waitingUpdate: RequestUpdate<unknown> = {
  status: LoadResultStatus.WAITING
};

const retryingUpdate: RequestUpdate<unknown> = {
  status: LoadResultStatus.RETRYING
};

export function loadWithRetry<TriggerPayload, Result>(
  loadFn: (triggerValue: TriggerPayload) => Observable<Result>,
  refreshTrigger?: Observable<TriggerPayload>,
  opts?: LoadWithRetryOptions
): Observable<RequestUpdate<Result>> {
  const options = { ...DEFAULT_OPTIONS, ...opts };
  const trigger: Observable<TriggerPayload> =
    refreshTrigger || of(undefined as unknown as TriggerPayload);
  return loadAttempt(loadFn, trigger, options, 1);
}

function loadAttempt<TriggerPayload, Result>(
  loadFn: (triggerValue: TriggerPayload) => Observable<Result>,
  refreshTrigger: Observable<TriggerPayload>,
  opts: LoadWithRetryOptions,
  attempts: number
): Observable<RequestUpdate<Result>> {
  return createTriggerPipeline(refreshTrigger, loadFn).pipe(
    tap(() => (attempts = 1)),
    catchError(error =>
      createTriggerErrorPipeline(error, loadFn, refreshTrigger, opts, attempts)
    )
  );
}

function createTriggerPipeline<TriggerPayload, Result>(
  refreshTrigger: Observable<TriggerPayload>,
  loadFn: (triggerValue: TriggerPayload) => Observable<Result>
) {
  return refreshTrigger.pipe(
    switchMap(triggerValue =>
      merge(
        of(initialStatus as RequestUpdate<Result>),
        loadFn(triggerValue).pipe(map(mapResultToSuccessUpdate))
      )
    )
  );
}

function createTriggerErrorPipeline<TriggerPayload, Result>(
  error: Error,
  loadFn: (triggerValue: TriggerPayload) => Observable<Result>,
  refreshTrigger: Observable<TriggerPayload>,
  opts: LoadWithRetryOptions,
  attempts: number
) {
  const errorUpdate: Observable<RequestUpdate<Result>> = of({
    status: LoadResultStatus.ERROR,
    error,
    willRetry: attempts < opts.attempts
  });

  const retryAttempt = createRetryAttempt(
    loadFn,
    refreshTrigger,
    opts,
    attempts
  );

  if (attempts === opts.attempts) {
    return errorUpdate;
  } else {
    return merge(errorUpdate, retryAttempt);
  }
}

function createRetryAttempt<TriggerPayload, Result>(
  loadFn: (triggerValue: TriggerPayload) => Observable<Result>,
  refreshTrigger: Observable<TriggerPayload>,
  opts: LoadWithRetryOptions,
  attempts: number
) {
  const delayedRetry = timer(calculateDelay(opts, attempts)).pipe(
    take(1),
    switchMap(() =>
      merge(
        of(retryingUpdate as RequestUpdate<Result>),
        loadAttempt(loadFn, refreshTrigger, opts, attempts + 1)
      )
    )
  );

  return merge(of(waitingUpdate as RequestUpdate<Result>), delayedRetry);
}

function mapResultToSuccessUpdate<Result>(
  results: Result
): RequestUpdate<Result> {
  return { status: LoadResultStatus.SUCCESS, results };
}

function calculateDelay(
  options: LoadWithRetryOptions,
  attempt: number
): number {
  const jitter = (Math.random() - 0.5) * options.retryDelayMs * 0.5;
  const delay =
    options.retryDelayMs *
      Math.pow(options.retryBackoffCoefficient, attempt - 1) +
    jitter;
  return Math.min(delay, options.retryMaxDelayMs);
}

/**
 * This operator may be used to automatically retrieve the 'results'
 * of a successful 'loadWithRetry' operation.
 *
 * However, only use this operator if you do not care about the
 * intermediate state of the LoardResultStatus, as this will filter
 * out everything besides the SUCCESS state;
 * states such as 'IN_PROGRESS' or 'ERROR' will not be available
 * should those be needed for presentation logic.
 */
export function waitForResults() {
  return <T>(source: Observable<RequestUpdate<T>>) =>
    source.pipe(
      filter(res => res.status === LoadResultStatus.SUCCESS),
      map(res => (res as Success<T>).results)
    );
}

export function waitForError() {
  return (source: Observable<RequestUpdate<unknown>>) =>
    source.pipe(
      filter(
        (res): res is RequestError => res.status === LoadResultStatus.ERROR
      )
    );
}

export function whenErrored<T>(
  errorHandler: (requestError: RequestError) => T
) {
  return (source: Observable<RequestUpdate<T>>) =>
    source.pipe(
      switchMap(requestUpdate => {
        if (requestUpdate.status === LoadResultStatus.ERROR) {
          const newPayload: T = errorHandler(requestUpdate);
          return of({
            status: LoadResultStatus.SUCCESS,
            results: newPayload
          } as RequestUpdate<T>);
        } else {
          return of(requestUpdate);
        }
      })
    );
}
