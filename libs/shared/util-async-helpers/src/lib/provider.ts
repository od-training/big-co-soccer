import { makeEnvironmentProviders, Type } from '@angular/core';

import { NotificationService } from './notification-service';

export function provideNotificationService(flavor: Type<NotificationService>) {
  return makeEnvironmentProviders([
    {
      provide: NotificationService,
      useClass: flavor
    }
  ]);
}
