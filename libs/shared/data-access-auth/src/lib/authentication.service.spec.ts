// These tests should exercise the services ability to
// marshal credentials from an app into a form suitable for the
// backing authentication service. They should also verify that
// responses from the authentication service are converted to a
// form that is suitable for generic use.

// Since we do not have a backing authentication service. These
// tests are missing a verification of the data transformation.

import { waitForAsync } from '@angular/core/testing';
// They should NOT
// * attempt to verify that a user can utilize a UI to supply credentials
// * verify that the backing authentication service correctly verifies a user
// * verify the application screen flow in response to login, logout
// and user changes
import { AuthenticationService } from './authentication.service';
import { first } from 'rxjs/operators';
import { AuthenticationConfig } from './types';

const mockUsername = 'testUser';
const mockPassword = 'testPassword';

describe('The data-access-auth library authentication service', () => {
  let authenticationService: AuthenticationService;
  const mockDevConfig: AuthenticationConfig = {
    dev: true,
    authenticationServerAddress: ''
  };

  const mockProdConfig: AuthenticationConfig = {
    dev: false,
    authenticationServerAddress: ''
  };
  describe('when in dev mode', () => {
    beforeEach(() => {
      authenticationService = new AuthenticationService(mockDevConfig);
    });
    it('should publish the logged in user upon login', waitForAsync(async () => {
      authenticationService.currentUser.pipe(first()).subscribe(user => {
        expect(user).not.toBeNull();
        expect(user?.username).toBe('testUser');
      });
      await authenticationService.login(mockUsername, mockPassword);
    }));
    it('should clear the logged in user if an invalid request is made', waitForAsync(async () => {
      await authenticationService.login(mockUsername, mockPassword);

      // Subscribe after we login so that the next user change is associated with
      // logging out.
      authenticationService.currentUser.pipe(first()).subscribe(user => {
        expect(user).toBeNull();
      });

      // Catch the login error so that the tests don't freak out
      try {
        await authenticationService.login('', '');
      } catch (err) {
        expect(err).toEqual(
          new Error('password and username are required when logging in')
        );
      }
    }));
  });
  describe('when not in dev mode', () => {
    beforeEach(() => {
      authenticationService = new AuthenticationService(mockProdConfig);
    });
    it('should always clear user upon login', waitForAsync(() => {
      authenticationService.currentUser.pipe(first()).subscribe(user => {
        expect(user).toBeNull();
      });
      expect(
        authenticationService.login(mockUsername, mockPassword)
      ).rejects.toThrow('password and username are required when logging in');
    }));
  });
  describe('when logging out', () => {
    beforeEach(() => {
      authenticationService = new AuthenticationService(mockDevConfig);
    });
    it('should clear the user upon logout', waitForAsync(async () => {
      await authenticationService.login(mockUsername, mockPassword);
      authenticationService.currentUser.pipe(first()).subscribe(user => {
        expect(user).toBeNull();
      });
      authenticationService.logout();
    }));
  });
});
