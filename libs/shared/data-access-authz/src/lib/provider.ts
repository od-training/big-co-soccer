import { makeEnvironmentProviders } from '@angular/core';
import { AuthorizationConfig } from './types';
import { AuthorizationConfigToken } from './tokens';

/**
 * This library encapsulates the details of authorization (authz)
 * Authentication (auth) is handled separately.
 *
 * Specifically, it handles the details related to providing
 * authorization of route and API access. Specifically, it handles
 * requests for authorization by the application based on the current
 * user and desired route supplied to a method. For API access it
 * exposes methods that can be used from interceptors to attach
 * the user identity to a request.
 */
export function provideAuthz(config: AuthorizationConfig) {
  return makeEnvironmentProviders([
    // Capture the provided configuration data in an Injection Token
    {
      provide: AuthorizationConfigToken,
      useValue: config
    }
  ]);
}
