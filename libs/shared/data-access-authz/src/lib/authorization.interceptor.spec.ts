// Test only the addHeaders method as the intercept method
// contains only boilerplate. Apply traditional techniques for testing
// the method as desired.

// This is a good example of the argument
// for leveraging Angular to aid in testing.

describe('The data-access-authz library interceptor logic', () => {
  // At the moment addHeaders doesn't actually do anything.
  // This is a placeholder
  it('should contain a placeholder test', () => {
    expect(true).toBe(true);
  });
});
