import { Inject, Injectable } from '@angular/core';

import { AuthenticationService, User } from '@bsc/shared/data-access-auth';

import { AuthorizationConfigToken } from './tokens';
import { AuthorizationConfig } from './types';

@Injectable({ providedIn: 'root' })
/**
 * Provides methods for determining access to features
 */
export class AuthorizationService {
  constructor(
    @Inject(AuthorizationConfigToken) private config: AuthorizationConfig,
    private authenticationService: AuthenticationService
  ) {}

  /**
   * checkRouteAccess will look at the dev flag of the configuration object.
   * If in dev mode, it will delegate the access check to the dev mode function.
   * Otherwise it will use the prod mode function.
   *
   * @param route The route to determine if access should be granted. This is typically
   *              supplied by the framework
   * @returns A boolean response indicating if the user has access to the route
   */
  checkRouteAccess(route: string): boolean {
    if (this.config.dev) {
      return verifyDevAccess(
        route,
        this.authenticationService.currentUserSnapshot
      );
    }

    return verifyProdAccess(
      route,
      this.authenticationService.currentUserSnapshot
    );
  }
}

/** verifyDevAccess will look at a user and route name to check to see
 * if the user has access to the route.
 *
 * In a real project this could be achieved a number of ways, mostly
 * dependent on a combination of the identity server and the business
 * needs. If the server needs to verify every request then HTTP would
 * be used and an observable would be returned.
 *
 * For this example we will assume that the user identity definition
 * contains sufficient information to validate the request.
 *
 * Note that this function does not contain any knowledge of routing structure,
 * or what to do if a request is denied. It has the responsibility for
 * simply providing the "yes" or "no" answer or passing the answer back
 * from an authorization service.
 *
 * This is typically called by the Auth Service when it is configured
 * in dev mode. A sibling of verifyProdAccess.
 * @param route: The route name to validate access for. Typically the
 * name of the leaf active route.
 * @param currentUser: The user to validate access for. Typically the
 * logged in user.
 * @return Boolean answer to the question of if the specified user has
 * access to a route of the supplied name
 */
export function verifyDevAccess(
  route: string,
  currentUser: User | null
): boolean {
  // For now, allow all access in dev mode once the user is logged in
  return !!(route && currentUser);
}

/** verifyProdAccess will look at a user and route name to check to see
 * if the user has access to the route.
 *
 * In a real project this could be achieved a number of ways, mostly
 * dependent on a combination of the identity server and the business
 * needs. If the server needs to verify every request then HTTP would
 * be used and an observable would be returned.
 *
 * For this example we will assume that the user identity definition
 * contains sufficient information to validate the request.
 *
 * Note that this function does not contain any knowledge of routing structure,
 * or what to do if a request is denied. It has the responsibility for
 * simply providing the "yes" or "no" answer or passing the answer back
 * from an authorization service.
 *
 * This is typically called by the Auth Service when it is configured
 * in prod mode. A sibling of verifyDevAccess.
 * @param _route: The route name to validate access for. Typically the
 * name of the leaf active route.
 * @param _currentUser: The user to validate access for. Typically the
 * logged in user.
 * @return Boolean answer to the question of if the specified user has
 * access to a route of the supplied name
 */
export function verifyProdAccess(
  // Will likely be used once implemented
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _route: string,
  // Will likely be used once implemented
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _currentUser: User | null
): boolean {
  // No implementation yet, so deny everything.
  return false;
}
