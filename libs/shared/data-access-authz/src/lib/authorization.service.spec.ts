// Test for the ability to accept a route and return a boolean answer
// Test for correctness of the answer in the context of:
//   * performing the calculation locally AND
//   * unique route edge cases (special admin routes)
//   * unique user edge cases (no user yet)
//   * mock or stub
// Alternatively, if there are additional business rules in addition
// to server side rules, these should be tested as well.
// Do not test for server side correctness. (Stub out the server)

import {
  AuthenticationConfig,
  AuthenticationService,
  User
} from '@bsc/shared/data-access-auth';

import {
  AuthorizationService,
  verifyDevAccess,
  verifyProdAccess
} from './authorization.service';
import { AuthorizationConfig } from './types';

const testUsername = 'testUser';
const testRoute = 'test';

// Notice that these assertions bake assumptions
// of how the underlying "verifyDevAccess function works"
// These tests are actually integration tests

describe('The data-access-authz service integrations', () => {
  let authorizationService: AuthorizationService;
  let authenticationService: AuthenticationService;
  const testPassword = 'testPassword';

  const mockDevAuthzConfig: AuthorizationConfig = {
    dev: true,
    authorizationServerAddress: ''
  };

  const mockProdAuthzConfig: AuthorizationConfig = {
    dev: false,
    authorizationServerAddress: ''
  };

  const mockDevAuthConfig: AuthenticationConfig = {
    dev: true,
    authenticationServerAddress: ''
  };

  const mockProdAuthConfig: AuthenticationConfig = {
    dev: false,
    authenticationServerAddress: ''
  };

  describe('when in dev mode', () => {
    beforeEach(() => {
      // Setup dependencies in dev mode
      authenticationService = new AuthenticationService(mockDevAuthConfig);
      authorizationService = new AuthorizationService(
        mockDevAuthzConfig,
        authenticationService
      );
      authenticationService.login(testUsername, testPassword);
    });

    it('should verify access using dev mode functionality', () => {
      expect(authorizationService.checkRouteAccess(testRoute)).toBe(true);
    });
  });

  describe('when in prod mode', () => {
    beforeEach(() => {
      authenticationService = new AuthenticationService(mockProdAuthConfig);
      authorizationService = new AuthorizationService(
        mockProdAuthzConfig,
        authenticationService
      );
    });

    it('should verify access using prod mode functionality', () => {
      expect(authorizationService.checkRouteAccess(testRoute)).toBe(false);
    });
  });
});

describe('The data-access-authz library prod and dev verifications', () => {
  const testUser: User = {
    username: testUsername,
    displayName: 'Test User',
    avatarUrl: ''
  };
  const emptyRoute = '';

  describe('when there is no user', () => {
    it('should deny access', () => {
      expect(verifyDevAccess(testRoute, null)).toBe(false);
      expect(verifyProdAccess(testRoute, null)).toBe(false);
    });
  });

  describe('when there is no route', () => {
    it('should deny access', () => {
      expect(verifyDevAccess(emptyRoute, testUser)).toBe(false);
      expect(verifyProdAccess(emptyRoute, testUser)).toBe(false);
    });
  });

  describe('when route and user are present', () => {
    it('should always allow dev mode requests', () => {
      expect(verifyDevAccess(testRoute, testUser)).toBe(true);
    });

    it('should always deny prod mode requests', () => {
      expect(verifyProdAccess(testRoute, testUser)).toBe(false);
    });
  });
});
