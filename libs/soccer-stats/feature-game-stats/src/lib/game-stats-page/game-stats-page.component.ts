import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';

import {
  AppState,
  getGames,
  requestInitialGameList,
  requestInitialPlayerList
} from '@bsc/soccer-stats/data-access-app-state';

import { AddGameComponent } from '../add-game/add-game.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { RouterLink, RouterOutlet } from '@angular/router';
import { AsyncPipe } from '@angular/common';
import { MatListModule } from '@angular/material/list';

@Component({
  selector: 'bsc-game-stats-page',
  templateUrl: './game-stats-page.component.html',
  styleUrls: ['./game-stats-page.component.scss'],
  standalone: true,
  imports: [
    MatListModule,
    RouterLink,
    MatButtonModule,
    MatIconModule,
    RouterOutlet,
    AsyncPipe
  ]
})
export class GameStatsPageComponent {
  games = this.store.pipe(select(getGames));

  constructor(
    private store: Store<AppState>,
    private dialog: MatDialog
  ) {
    store.dispatch(requestInitialGameList());
    store.dispatch(requestInitialPlayerList());
  }

  addGame() {
    this.dialog.open(AddGameComponent);
  }
}
