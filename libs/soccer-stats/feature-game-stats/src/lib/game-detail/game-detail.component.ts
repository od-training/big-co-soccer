import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { map, tap } from 'rxjs/operators';

import {
  initiateDeleteGameRequest,
  getDeleteGameStatus,
  getPlayersNotInGame,
  getSelectedGame,
  deleteGameStatus,
  AppState,
  selectedGameIdChanged,
  ackDeleteGameStatus
} from '@bsc/soccer-stats/data-access-app-state';

import { selectedGameIdRouteParamName } from '../constants';
import { CardListComponent } from '../card-list/card-list.component';
import { ShotListComponent } from '../shot-list/shot-list.component';
import { PlayerListComponent } from '../player-list/player-list.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AsyncPipe, DatePipe } from '@angular/common';
import { TranslocoDirective } from '@ngneat/transloco';

@Component({
  selector: 'bsc-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    PlayerListComponent,
    ShotListComponent,
    CardListComponent,
    AsyncPipe,
    DatePipe
  ]
})
export class GameDetailComponent implements OnDestroy {
  gameIdSub = this.ar.params
    .pipe(map(params => params[selectedGameIdRouteParamName]))
    .subscribe(id => {
      this.store.dispatch(selectedGameIdChanged({ selectedGameId: id }));
      this.selectedGameId = id;
    });

  deleteGameStatus = deleteGameStatus;
  gameDetails = this.store.pipe(select(getSelectedGame));
  deleting = this.store.pipe(select(getDeleteGameStatus)).pipe(
    tap(status => {
      if (status === deleteGameStatus.deleteGameFinished) {
        this.router.navigate(['games']);
        this.store.dispatch(ackDeleteGameStatus());
      }
    })
  );
  playersNotInGame = this.store.pipe(select(getPlayersNotInGame));
  private selectedGameId: string | undefined;
  constructor(
    private ar: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>
  ) {}

  delete() {
    if (this.selectedGameId) {
      this.store.dispatch(
        initiateDeleteGameRequest({ gameId: this.selectedGameId })
      );
    }
  }

  ngOnDestroy() {
    this.gameIdSub.unsubscribe();
  }
}
