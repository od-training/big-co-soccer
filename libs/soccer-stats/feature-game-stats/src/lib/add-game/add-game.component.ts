import { Component, OnDestroy } from '@angular/core';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import {
  MatDialogActions,
  MatDialogContent,
  MatDialogRef
} from '@angular/material/dialog';

import {
  ackAddGameStatus,
  addGameStatusMessages,
  AppState,
  getAddGameStatus,
  initiateAddGameRequest
} from '@bsc/soccer-stats/data-access-app-state';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AsyncPipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslocoDirective } from '@ngneat/transloco';
import { MatDateFnsModule } from '@angular/material-date-fns-adapter';
import { format } from 'date-fns';

@Component({
  selector: 'bsc-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    MatDateFnsModule,
    MatDialogContent,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatDialogActions,
    MatButtonModule,
    MatProgressSpinnerModule,
    AsyncPipe
  ]
})
export class AddGameComponent implements OnDestroy {
  addGameStatus = this.store.select(getAddGameStatus);
  locationInput = new FormControl('', {
    validators: Validators.required,
    nonNullable: true
  });
  dateInput = new FormControl(new Date(), {
    validators: Validators.required,
    nonNullable: true
  });
  nameInput = new FormControl('', {
    validators: Validators.required,
    nonNullable: true
  });
  addGameSub: Subscription;
  addGameStatusMessages = addGameStatusMessages;

  constructor(
    dialogRef: MatDialogRef<AddGameComponent>,
    private store: Store<AppState>
  ) {
    this.addGameSub = this.addGameStatus
      .pipe(filter(status => status === addGameStatusMessages.addGameSuccess))
      .subscribe(() => {
        this.store.dispatch(ackAddGameStatus());
        dialogRef.close();
      });
  }

  ngOnDestroy(): void {
    this.addGameSub.unsubscribe();
  }

  save() {
    this.store.dispatch(
      initiateAddGameRequest({
        name: this.nameInput.value,
        date: format(this.dateInput.value, 'yyy-MM-dd'),
        location: this.locationInput.value,
        players: [] as string[]
      })
    );
  }
}
