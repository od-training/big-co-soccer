import { Component, Inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogContent,
  MatDialogActions
} from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import {
  ackAddShotStatus,
  AppState,
  initiateAddShotRequest,
  getAddShotStatus,
  addShotStatusMessages
} from '@bsc/soccer-stats/data-access-app-state';

import { GameModalTransfer } from '../types';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AsyncPipe } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslocoDirective } from '@ngneat/transloco';

function CantAssistYourselfValidator(group: FormGroup) {
  const player = group.get('player');
  const assist = group.get('assist');
  if (player && assist && player.value === assist.value) {
    return {
      cantAssistYourself: true
    };
  }
  return null;
}

@Component({
  selector: 'bsc-add-shot-to-game',
  templateUrl: './add-shot-to-game.component.html',
  styleUrls: ['./add-shot-to-game.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    ReactiveFormsModule,
    MatDialogContent,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatDialogActions,
    MatButtonModule,
    MatProgressSpinnerModule,
    AsyncPipe
  ]
})
export class AddShotToGameComponent {
  addShotStatus = this.store.select(getAddShotStatus);
  shotForm = this.fb.group(
    {
      player: ['', Validators.required],
      assist: [''],
      scored: [true, Validators.required],
      minute: [0, Validators.required]
    },
    {
      validators: [CantAssistYourselfValidator]
    }
  );
  addShotSub: Subscription;
  addShotStatusMessages = addShotStatusMessages;

  constructor(
    dialogRef: MatDialogRef<AddShotToGameComponent>,
    private fb: FormBuilder,
    private store: Store<AppState>,
    @Inject(MAT_DIALOG_DATA) public game: GameModalTransfer
  ) {
    this.addShotSub = this.addShotStatus
      .pipe(
        filter(status => status === this.addShotStatusMessages.addShotSuccess)
      )
      .subscribe(() => {
        store.dispatch(ackAddShotStatus());
        dialogRef.close();
      });
  }

  save() {
    this.store.dispatch(
      initiateAddShotRequest({
        game: this.game.id,
        ...this.shotForm.value
      })
    );
  }
}
