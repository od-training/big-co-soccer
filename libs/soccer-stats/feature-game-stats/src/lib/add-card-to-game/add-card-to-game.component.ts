import { Component, Inject } from '@angular/core';
import {
  NonNullableFormBuilder,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogContent,
  MatDialogActions
} from '@angular/material/dialog';

import {
  ackAddCardStatus,
  addCardStatusMessages,
  AppState,
  cardTypes,
  getAddCardStatus,
  initiateAddCardRequest
} from '@bsc/soccer-stats/data-access-app-state';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { cardTypesList } from '../constants';
import { GameModalTransfer } from '../types';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { AsyncPipe } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslocoDirective } from '@ngneat/transloco';

@Component({
  selector: 'bsc-add-card-to-game',
  templateUrl: './add-card-to-game.component.html',
  styleUrls: ['./add-card-to-game.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    ReactiveFormsModule,
    MatDialogContent,
    MatFormFieldModule,
    MatInputModule,
    MatDialogActions,
    MatButtonModule,
    MatProgressSpinnerModule,
    AsyncPipe
  ]
})
export class AddCardToGameComponent {
  addCardStatus = this.store.select(getAddCardStatus);
  cardForm = this.fb.group({
    player: ['', Validators.required],
    cardType: [cardTypes.yellow],
    minute: [0, Validators.required]
  });
  addCardSub: Subscription;
  addCardStatusMessages = addCardStatusMessages;
  cardtypes = cardTypesList;

  constructor(
    dialogRef: MatDialogRef<AddCardToGameComponent>,
    private fb: NonNullableFormBuilder,
    private store: Store<AppState>,
    @Inject(MAT_DIALOG_DATA) public game: GameModalTransfer
  ) {
    this.addCardSub = this.addCardStatus
      .pipe(
        filter(status => status === this.addCardStatusMessages.addCardSuccess)
      )
      .subscribe(() => {
        store.dispatch(ackAddCardStatus());
        dialogRef.close();
      });
  }

  async save() {
    this.store.dispatch(
      initiateAddCardRequest({
        game: this.game.id,
        player: this.cardForm.controls.player.value,
        cardType: this.cardForm.controls.cardType.value,
        minute: this.cardForm.controls.minute.value
      })
    );
  }
}
