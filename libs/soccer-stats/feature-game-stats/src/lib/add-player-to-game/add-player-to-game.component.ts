import { Component, Inject } from '@angular/core';
import { FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogContent,
  MatDialogActions
} from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import {
  ackAddPlayerToGameStatus,
  addPlayerToGameStatusMessages,
  AppState,
  getAddPlayerToGameStatus,
  initiateAddPlayerToGameRequest
} from '@bsc/soccer-stats/data-access-app-state';

import { GameModalTransfer } from '../types';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonModule } from '@angular/material/button';
import { AsyncPipe } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslocoDirective } from '@ngneat/transloco';

@Component({
  selector: 'bsc-add-player-to-game',
  templateUrl: './add-player-to-game.component.html',
  styleUrls: ['./add-player-to-game.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    MatDialogContent,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogActions,
    MatButtonModule,
    MatProgressSpinnerModule,
    AsyncPipe
  ]
})
export class AddPlayerToGameComponent {
  playerOptions = this.game.players;
  addPlayerStatus = this.store.select(getAddPlayerToGameStatus);
  chosenPlayer = new FormControl('', {
    validators: Validators.required,
    nonNullable: true
  });
  addPlayerSub: Subscription;
  addPlayerToGameStatusMessages = addPlayerToGameStatusMessages;

  constructor(
    dialogRef: MatDialogRef<AddPlayerToGameComponent>,
    private store: Store<AppState>,
    @Inject(MAT_DIALOG_DATA) public game: GameModalTransfer
  ) {
    this.addPlayerSub = this.addPlayerStatus
      .pipe(
        filter(
          status =>
            status === addPlayerToGameStatusMessages.addPlayerToGameSuccess
        )
      )
      .subscribe(() => {
        store.dispatch(ackAddPlayerToGameStatus());
        dialogRef.close();
      });
  }

  async save() {
    this.store.dispatch(
      initiateAddPlayerToGameRequest({
        game: this.game.id,
        player: this.chosenPlayer.value
      })
    );
  }
}
