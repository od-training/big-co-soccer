import { Routes } from '@angular/router';

import { selectedGameIdRouteParamName } from './constants';
import { GameStatsPageComponent } from './game-stats-page/game-stats-page.component';
import { GameDetailComponent } from './game-detail/game-detail.component';

export const routes: Routes = [
  {
    path: '',
    component: GameStatsPageComponent,
    children: [
      {
        path: `:${selectedGameIdRouteParamName}`,
        component: GameDetailComponent
      }
    ]
  }
];
