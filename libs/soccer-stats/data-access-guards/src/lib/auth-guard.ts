import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, Router, UrlTree } from '@angular/router';

import { AuthorizationService } from '@bsc/shared/data-access-authz';

export function authorizationGuard(
  route: ActivatedRouteSnapshot
): boolean | UrlTree {
  const path = buildPathFromSegments(extractSegmentsFromSnapshot(route));

  return checkUrlAccess(inject(AuthorizationService), inject(Router), path);
}

export function checkUrlAccess(
  authorizationService: AuthorizationService,
  router: Router,
  path: string
) {
  const authorized = authorizationService.checkRouteAccess(path);
  return authorized || router.parseUrl('/login');
}

export function extractSegmentsFromSnapshot(
  activatedRoute: ActivatedRouteSnapshot
): string[] {
  return activatedRoute.url.map(segment => segment.path);
}

export function buildPathFromSegments(urls: string[]): string {
  let fullPath = '';
  urls.forEach(segment => (fullPath += `/${segment}`));
  return fullPath;
}
