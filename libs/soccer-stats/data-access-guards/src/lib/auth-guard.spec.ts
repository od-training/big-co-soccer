// Test the buildPath function
// The integration testing of the guard should be tested in E2E

import { ActivatedRouteSnapshot, Router, UrlSegment } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TestBed } from '@angular/core/testing';
import { AuthorizationService } from '@bsc/shared/data-access-authz';

import {
  buildPathFromSegments,
  checkUrlAccess,
  extractSegmentsFromSnapshot
} from './auth-guard';

const goodPath = '/path/to/target';
const fragments: string[] = ['path', 'to', 'target'];
const paths: Partial<UrlSegment>[] = [
  { path: 'path' },
  { path: 'to' },
  { path: 'target' }
];
const segments: Partial<ActivatedRouteSnapshot> = {
  url: paths as UrlSegment[]
};
const badPath = '/path/to/failure';

describe('The Soccer Stats data-access-guards', () => {
  describe('checkUrlAccess', () => {
    let router: Router;
    let authService: AuthorizationService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule.withRoutes([])],
        providers: [
          {
            provide: AuthorizationService,
            useValue: {
              checkRouteAccess: (path: string) => {
                // Allow easy testing of good and bad paths
                return path === goodPath;
              }
            }
          }
        ]
      });

      router = TestBed.inject(Router);
      authService = TestBed.inject(AuthorizationService);
    });

    it('should return true for approved paths', () => {
      expect(checkUrlAccess(authService, router, goodPath)).toBe(true);
    });

    it('should return a new URL tree for unapproved paths', () => {
      expect(checkUrlAccess(authService, router, badPath)).not.toBe(true);
    });
  });

  describe('path parsing utilities', () => {
    it('should form a url out of segments', () => {
      expect(buildPathFromSegments(fragments)).toEqual(goodPath);
    });

    it('should convert segments to strings', () => {
      expect(
        extractSegmentsFromSnapshot(segments as ActivatedRouteSnapshot)
      ).toEqual(fragments);
    });
  });
});
