import { User } from '@bsc/shared/data-access-auth';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getCurrentUser = createSelector(
  createFeatureSelector<User | null>('user'),
  user => user
);
