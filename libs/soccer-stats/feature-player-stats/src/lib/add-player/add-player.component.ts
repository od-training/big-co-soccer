import { Component, OnDestroy } from '@angular/core';
import { FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import {
  MatDialogRef,
  MatDialogContent,
  MatDialogActions
} from '@angular/material/dialog';
import { Store } from '@ngrx/store';

import {
  ackAddPlayerStatus,
  AppState,
  initiateAddPlayerRequest,
  addPlayerStatusMessages,
  getAddPlayerStatus
} from '@bsc/soccer-stats/data-access-app-state';
import { filter } from 'rxjs/operators';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { AsyncPipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslocoDirective } from '@ngneat/transloco';

@Component({
  selector: 'bsc-add-player',
  templateUrl: './add-player.component.html',
  styleUrls: ['./add-player.component.scss'],
  standalone: true,
  imports: [
    TranslocoDirective,
    MatDialogContent,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogActions,
    MatButtonModule,
    MatProgressSpinnerModule,
    AsyncPipe
  ]
})
export class AddPlayerComponent implements OnDestroy {
  playerInput = new FormControl('', {
    validators: Validators.required,
    nonNullable: true
  });
  addPlayerStatus = this.store.select(getAddPlayerStatus);
  addPlayerStatusMessages = addPlayerStatusMessages;
  addPlayerSub = this.addPlayerStatus
    .pipe(
      filter(status => status === this.addPlayerStatusMessages.addPlayerSuccess)
    )
    .subscribe(() => {
      this.store.dispatch(ackAddPlayerStatus());
      this.dialogRef.close();
    });

  constructor(
    private dialogRef: MatDialogRef<AddPlayerComponent>,
    private store: Store<AppState>
  ) {}

  ngOnDestroy(): void {
    this.addPlayerSub.unsubscribe();
  }

  save() {
    this.store.dispatch(
      initiateAddPlayerRequest({ player: this.playerInput.value })
    );
  }
}
