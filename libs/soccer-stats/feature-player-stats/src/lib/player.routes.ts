import { Routes } from '@angular/router';

import { PlayerStatsPageComponent } from './player-stats-page/player-stats-page.component';
import { selectedPlayerIdRouteParamName } from './constants';
import { PlayerDetailComponent } from './player-detail/player-detail.component';

export const routes: Routes = [
  {
    path: '',
    component: PlayerStatsPageComponent,
    children: [
      {
        path: `:${selectedPlayerIdRouteParamName}`,
        component: PlayerDetailComponent
      }
    ]
  }
];
