import simpleGit, {
  DefaultLogFields,
  ListLogLine,
  SimpleGit
} from 'simple-git';

const git: SimpleGit = simpleGit();

async function go() {
  const logs = await git.log();
  const logsInChronologicalOrder = [...logs.all].reverse();

  let majorPhase = 1; // Paul wants the major phase to start at 2
  let minorPhase = 0;

  for await (const commit of logsInChronologicalOrder) {
    if (commit.message.includes('[MAJOR]') || commit.body.includes('[MAJOR]')) {
      majorPhase++;
      minorPhase = 1;
      await tagCommit(commit, majorPhase, minorPhase);
    }
    if (commit.message.includes('[MINOR]') || commit.body.includes('[MINOR]')) {
      minorPhase++;
      await tagCommit(commit, majorPhase, minorPhase);
    }
  }
}

async function tagCommit(
  commit: DefaultLogFields & ListLogLine,
  majorPhase: number,
  minorPhase: number
) {
  await git.tag([`phase-${majorPhase}-${minorPhase}`, commit.hash, '--force']);
}

go();
